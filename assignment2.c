#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num){
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (!newNode) {
        printf("Memory error\n");
        return NULL;
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head){
    struct Node *current = head;
    while (current != NULL) {
        printf("%d ", current->number);
        current = current->next;
    }
    printf("\n");
}

void append(struct Node **head, int num){
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *last = *head;
    while (last->next != NULL) {
        last = last->next;
    }
    last->next = newNode;
}


void prepend(struct Node **head, int num){

    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;

}

void deleteByKey(struct Node **head, int key){
    struct Node *temp = *head, *prev;
    if (temp != NULL && temp->number == key) {
        *head = temp->next;
        free(temp);
        return;
    }
    while (temp != NULL && temp->number != key) {
        prev = temp;
        temp = temp->next;
    }
    if (temp == NULL) return;
    prev->next = temp->next;
    free(temp);
}


void deleteByValue(struct Node **head, int value){
     struct Node* current = *head;
    struct Node* next;
    while (current != NULL) {
        if (current->number == value) {
            next = current->next;
            free(current);
            current = next;
        } else {
            current = current->next;
        }
    }
}
void insertAfterKey(struct Node **head, int key, int value){
    struct Node *newNode = createNode(value);
    struct Node *current = *head;
    while (current != NULL) {
        if (current->number == key) {
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }
    printf("Key not found\n");
}

void insertAfterValue(struct Node **head, int searchValue, int newValue){
    struct Node *newNode = createNode(newValue);
    struct Node *current = *head;
    while (current != NULL) {
        if (current->number == searchValue) {
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }
    printf("Value not found\n");
}

int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

         switch (choice) {
            case  1:
                printList(head);
                break;
            case  2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case  3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case  4:
                printf("Enter data to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case  5:
                return  0;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }
    return  0;
}
