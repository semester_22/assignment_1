#include<stdio.h>


int main(){
    //user to input values of coefficeints
    int a;
    int b;
    int c;
   

    printf("enter the values of the coefficients a b and c\n");
    printf("what is a\n?");
    scanf("%d", &a);

    if(a>0){
        printf("your value of a is valid");
    }
    else {
        printf("Please enter another value greater than 0\n");
    }

    printf("what is b?\n");
    scanf("%d", &b);

    printf("what is c?\n");
    scanf("%d", &c);

    //function to calculate discriminant
    int result = (b*b - 4*a*c);
    printf("the discriminant is:%d\n", result);
    if (result>0){
        
        printf("COMPLEX");
    }
    else if (result<0){
        printf("two complex conjugate roots\n");
    }
    else {
        printf("one real rfoot with repeated roots\n");
    }

    return 0;
}